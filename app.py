#from flask_jwt import JWT
from flask import Flask
from flask_cors import CORS

from config import app, api
from ressources.user import UserRegister, UserListe, User, UserUpdate
from ressources.persons import PersonRegister, PersonListe, Person, PersonUpdate
from ressources.tourismes import TourismeRegister, TourismeListe, Tourisme, TourismeUpdate
from ressources.musees import MuseeRegister, MuseeListe, Musee, MuseeUpdate
from ressources.parcs import ParcRegister, ParcListe, Parc, ParcUpdate
from ressources.cultures import CultureRegister, CultureListe, Culture, CultureUpdate


cors = CORS(app, resources={
    r'/*': {
        'origins': '*'
    }
})
app.config['CORS_HEADERS'] = 'Content-Type'






#les routes users
api.add_resource(UserRegister, '/register') # Pour enregistrer un nouveau useur
api.add_resource(User, '/user/<string:username>') #Pour voir et supprimer un user
api.add_resource(UserUpdate, '/user/<int:id>') #Pour modifier  un user
api.add_resource(UserListe, '/users') #Pour voir tous les users

#les routes personnalité
api.add_resource(PersonRegister, '/personnalite/register') # Pour enregistrer une nouvelle personnalité
api.add_resource(Person, '/personnalite/<string:nom>') #Pour voir et supprimer une personnalité
api.add_resource(PersonUpdate, '/personnalite/<int:id>') #Pour modifier une personnalité par son id
api.add_resource(PersonListe, '/personnalites') #Pour voir toutes les personnalités

#les routes tourisme
api.add_resource(TourismeRegister, '/site-tourisque/register') # Pour enregistrer un nouveau site touristique
api.add_resource(Tourisme, '/site-tourisque/<string:nom>') #Pour voir et supprimer un site touristique
api.add_resource(TourismeUpdate, '/site-tourisque/<int:id>') #Pour modifier un site touristique par son id
api.add_resource(TourismeListe, '/sites-tourisques') #Pour voir tous les sites touristiques

#les routes musée
api.add_resource(MuseeRegister, '/musee/register') # Pour enregistrer un nouveau musée
api.add_resource(Musee, '/musee/<string:nom>') #Pour voir et supprimer un musée
api.add_resource(MuseeUpdate, '/musee/<int:id>') #Pour modifier un musée par son id
api.add_resource(MuseeListe, '/musees') #Pour voir tous les musées

#les routes parc
api.add_resource(ParcRegister, '/parc/register')
api.add_resource(Parc, '/parc/<string:nom>')
api.add_resource(ParcUpdate, '/parc/<int:id>') 
api.add_resource(ParcListe, '/parcs')

#les routes culture
api.add_resource(CultureRegister, '/culture/register')
api.add_resource(Culture, '/culture/<string:nom>')
api.add_resource(CultureUpdate, '/culture/<int:id>') 
api.add_resource(CultureListe, '/cultures')




if __name__ == '__main__':
    app.run(debug=True)