from config import db


class UserModel(db.Model):
    __tablename__="users"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), nullable=False, unique=True)
    email = db.Column(db.String(120), nullable=False, unique=True)
    password = db.Column(db.String(120), nullable=False)
    
    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password
    
    def json(self):
        return {"username": self.username, "email":self.email,"password": self.password}

    def __repr__(self):
        return '<User %r>' % self.username

    #création d'un utilisateur dans la base de donnée
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    

    #supression d'un utilisateur dans la base de donnée
    def delete_to_db(self):
        db.session.delete(self)
        db.session.commit()


    #recherche d'un user par le username
    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username = username).first()


    #recherche d'un user par le id
    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id = id).first()
    
    #recherche d'un user par son email
    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email = email).first()
