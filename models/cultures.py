from config import db

class CultureModel(db.Model):
    __tablename__="culture"
    id = db.Column(db.Integer, primary_key=True)
    categorie = db.Column(db.String(120), nullable=False)
    nom = db.Column(db.String(250),nullable=False, unique=False)
    infos = db.Column(db.Text, nullable=False, unique=False)
    description = db.Column(db.Text, nullable=False, unique=False)
    img_url = db.Column(db.String(250), nullable=False, unique=True)

    def __init__(self, categorie, nom, infos, description, img_url):
        self.categorie = categorie
        self.nom = nom
        self.infos = infos
        self.description = description
        self.img_url = img_url
    
    def json(self):
        return {
            "categorie":self.categorie,
            "nom":self.nom,
            "infos":self.infos,
            "description":self.description,
            "img_url":self.img_url
        }
    
    def __repr__(self):
        return '<Culture %r>' % self.nom
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    

    def delete_to_db(self):
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def find_by_nom(cls, nom):
        return cls.query.filter_by(nom = nom).first()
    
    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id = id).first()