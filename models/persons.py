from config import db


class PersonModel(db.Model):
    __tablename__="persons"
    id = db.Column(db.Integer, primary_key=True)
    types = db.Column(db.String(120), nullable=False)
    nom = db.Column(db.String(120), nullable=False, unique=True)
    profession = db.Column(db.String(120), nullable=False)
    fonction_politique = db.Column(db.String(120))
    description = db.Column(db.String(255), nullable=False)
    date_naissance = db.Column(db.String(120), nullable=False)
    date_deces = db.Column(db.String(120))


    def __init__(self, types, nom, profession, fonction_politique, description, date_naissance, date_deces):
        self.types = types
        self.nom = nom
        self.profession = profession
        self.fonction_politique = fonction_politique
        self.description = description
        self.date_naissance = date_naissance
        self.date_deces = date_deces
    
    def json(self):
        return {
            "types":self.types,
            "nom":self.nom,
            "profession":self.profession,
            "fonction_politique":self.fonction_politique,
            "description":self.description,
            "date_naissance":self.date_naissance,
            "date_deces":self.date_deces
        }

    def __repr__(self):
        return '<Person %r>' % self.nom

    #enregistrement d'une personnalité dans la base de donnée
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    

    #supression d'une personnalité dans la base de donnée
    def delete_to_db(self):
        db.session.delete(self)
        db.session.commit()


    #recherche d'un user par le username
    @classmethod
    def find_by_nom(cls, nom):
        return cls.query.filter_by(nom = nom).first()


    #recherched'une personnalité par le id
    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id = id).first()
