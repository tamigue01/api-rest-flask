from config import db

class ParcModel(db.Model):
    __tablename__="parcs"
    id = db.Column(db.Integer, primary_key=True)
    categorie = db.Column(db.String(120), nullable=False)
    nom = db.Column(db.String(250),nullable=False, unique=False)
    adresse = db.Column(db.String(120), nullable=False, unique=False)
    contacts = db.Column (db.String(20), nullable=False, unique=True)
    description = db.Column(db.Text, nullable=False, unique=False)
    site_web = db.Column(db.String(120), nullable=False, unique=True)

    def __init__(self, categorie, nom, adresse, contacts, description, site_web):
        self.categorie = categorie
        self.nom = nom
        self.adresse = adresse
        self.contacts = contacts
        self.description = description
        self.site_web = site_web
    
    def json(self):
        return {
            "categorie":self.categorie,
            "nom":self.nom,
            "adresse":self.adresse,
            "contacts":self.contacts,
            "description":self.description,
            "site_web":self.site_web
        }
    
    def __repr__(self):
        return '<Parc %r>' % self.nom
    
    #enregistrement d'un parc dans la base de donnée
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    

    #supression d'un parc dans la base de donnée
    def delete_to_db(self):
        db.session.delete(self)
        db.session.commit()


    #recherche d'un parc par le nom
    @classmethod
    def find_by_nom(cls, nom):
        return cls.query.filter_by(nom = nom).first()
    
    #recherche d'un parc par le id
    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id = id).first()