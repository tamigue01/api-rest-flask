from config import db

class TourismeModel(db.Model):
    __tablename__="tourismes"
    id = db.Column(db.Integer, primary_key=True)
    types = db.Column(db.String(120), nullable=False)
    nom = db.Column(db.String(120), nullable=False, unique=True)
    details = db.Column(db.Text, nullable=False, unique=False)
    img = db.Column(db.String(250), nullable=False, unique=True)

    def __init__(self,types, nom, details, img):
        self.types = types
        self.nom = nom
        self.details = details
        self.img = img
    
    def json(self):
        return {"types":self.types, "nom": self.nom, "details":self.details,"img": self.img}

    def __repr__(self):
        return '<Tourisme %r>' % self.nom
    
    #Enregistrement d'un site touristique
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    

    #supression d'un site touristique
    def delete_to_db(self):
        db.session.delete(self)
        db.session.commit()


    #recherche d'un site touristique par son nom
    @classmethod
    def find_by_nom(cls, nom):
        return cls.query.filter_by(nom = nom).first()


    #recherche d'un site touristique par le id
    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id = id).first()