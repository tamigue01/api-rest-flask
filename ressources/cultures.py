from flask_restful import Resource, reqparse

from models.cultures import CultureModel


class CultureRegister(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('categorie', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('nom', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('infos', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('description', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('img_url', type = str, required = True, help = 'Ce champ est obligatoire')

    def post(self):
        data = CultureRegister.parser.parse_args()

        if (data['categorie']=="") or (data['nom']==""):
            return {"massage":"Veuillez remplir les champs 'categorie', 'nom' SVP !!!"}

        if CultureModel.find_by_nom(data['nom']):
            return {"massage":"Cette culture exsiste déja !!!"}, 400  #si le nom existe déja alors on ne peut plus l'utiliser
        
        cult = CultureModel(
            data['categorie'], 
            data['nom'], 
            data['infos'], 
            data['description'], 
            data['img_url']
        )
        
        try:
            cult.save_to_db()
            return {"massage":"Culture  enregistrée avec succès!!!"}, 201
        except:
            return {"massage":"L'enregistré de la culture  à échoué!!!"}, 500
    



class Culture(Resource):
    
    def get(self, nom):

        cult =  CultureModel.find_by_nom(nom)

        #Vérification si le nom existe
        if cult:
            return cult.json()
        else:
            return {"massage":"Cette culture n'exsiste pas !!!"}, 404

    #Suppression d'un musée  par son nom
    def delete(self, nom):
        cult =  CultureModel.find_by_nom(nom)
        
        #Vérification si le nom existe
        if cult:
            cult.delete_to_db()
            return {"massage":"Culture supprimé avec succès !!!"}
        else:
            return {"massage":"Cette culture n'exsiste pas !!!"}, 404


#pour fair la mise a jour d'un musée
class CultureUpdate(Resource):
    
    def put(self, id):
        
        cult = CultureModel.find_by_id(id)
        #si le id existe
        if cult:
            data = CultureRegister.parser.parse_args()

            if (data['categorie']=="") or (data['nom']==""):
                return {"massage":"Veuillez remplir les champs 'categorie', 'nom' SVP !!!"}
            
            cult.categorie = data['categorie']
            cult.nom = data['nom']
            cult.infos = data['infos'] 
            cult.contacts = data['contacts'] 
            cult.description = data['description'] 
            cult.img_url = data['img_url']

            try:
                cult.save_to_db()
                return {"massage":"Culture modifiée avec succès!!!"}, 201
            except:
                return {"massage":"La modification de la culture à échoué!!!"}, 500
        #si le id n'existe pas retourné un message d'eurreur
        else:
            return {"massage":"Cette culture n'exsiste pas !!!"}, 404
        

        


#pour afficher tous les users
class CultureListe(Resource):
    def get(self):
        return {"Cultures": list(map(lambda x: x.json(), CultureModel.query.all())) }

