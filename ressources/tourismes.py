from flask_restful import Resource, reqparse

from models.tourismes import TourismeModel


class TourismeRegister(Resource):
    parser = reqparse.RequestParser()

    parser.add_argument('types', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('nom', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('details', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('img', type = str, required = True, help = 'Ce champ est obligatoire')

    #la méthode post va nous permettre de créer un user
    def post(self):
        data = TourismeRegister.parser.parse_args()

        if (data['types']=="") or (data['nom']==""):
            return {"massage":"Veuillez remplir les champs 'types', 'nom' SVP !!!"}

        #Vérification pour éviter les doublons 
        if TourismeModel.find_by_nom(data['nom']):
            return {"massage":"Cet site touristique exsiste déja !!!"}, 400  #si le nom existe déja alors on ne peut plus l'utiliser
        
        tourism = TourismeModel(
            data['types'], 
            data['nom'], 
            data['details'], 
            data['img']
        )
        
        #Vérification de l'engeristrement dans la base de donnée
        try:
            tourism.save_to_db()
            return {"massage":"Site touristique enregistré avec succès!!!"}, 201
        except:
            return {"massage":"L'enregistrement du site touristique à échoué!!!"}, 500
    



class Tourisme(Resource):
    
    #Recherche d'un site touristique par son nom
    def get(self, nom):

        tourism =  TourismeModel.find_by_nom(nom)

        #Vérification si le nom existe
        if tourism:
            return tourism.json()
        else:
            return {"massage":"Cet site touristique n'exsiste pas !!!"}, 404

    #Suppression d'un site touristique  par son nom
    def delete(self, nom):
        tourism =  TourismeModel.find_by_nom(nom)
        
        #Vérification si le nom existe
        if tourism:
            tourism.delete_to_db()
            return {"massage":"Site touristique supprimé avec succès !!!"}
        else:
            return {"massage":"Cet site touristique n'existe n'exsiste pas !!!"}, 404


#pour fair la mise a jour d'un site touristique
class TourismeUpdate(Resource):
    
    def put(self, id):
        
        tourism = TourismeModel.find_by_id(id)
        #si le id existe
        if tourism:
            data = TourismeRegister.parser.parse_args()

            if (data['types']=="") or (data['nom']==""):
                return {"massage":"Veuillez remplir les champs 'types', 'nom' SVP !!!"}
            

            tourism.types = data['types']
            tourism.nom = data['nom']
            tourism.details = data['details'] 
            tourism.img = data['img']

            try:
                tourism.save_to_db()
                return {"massage":"Modification du site avec succès!!!"}, 201
            except:
                return {"massage":"La modification du site à échoué!!!"}, 500
        #si le id n'existe pas retourné un message d'eurreur
        else:
            return {"massage":"Cet site touristique n'exsiste pas !!!"}, 404
        

        


#pour afficher tous les site touristique
class TourismeListe(Resource):
    def get(self):
        return {"sites tourisques": list(map(lambda x: x.json(), TourismeModel.query.all())) }

