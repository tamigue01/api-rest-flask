from flask_restful import Resource, reqparse

from models.musees import MuseeModel


class MuseeRegister(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('categorie', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('nom', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('adresse', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('contacts', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('description', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('site_web', type = str, required = True, help = 'Ce champ est obligatoire')

    #la méthode post va nous permettre d'enregistrer un musee
    def post(self):
        data = MuseeRegister.parser.parse_args()

        if (data['categorie']=="") or (data['nom']=="") or (data['adresse']==""):
            return {"massage":"Veuillez remplir les champs 'categorie', 'nom', 'adresse' SVP !!!"}

        #Vérification pour éviter les doublons 
        if MuseeModel.find_by_nom(data['nom']):
            return {"massage":"Ce musée exsiste déja !!!"}, 400  #si le nom existe déja alors on ne peut plus l'utiliser
        
        mus = MuseeModel(
            data['categorie'], 
            data['nom'], 
            data['adresse'], 
            data['contacts'], 
            data['description'], 
            data['site_web']
        )
        
        #Vérification de l'engeristrement dans la base de donnée
        try:
            mus.save_to_db()
            return {"massage":"Musée  enregistré avec succès!!!"}, 201
        except:
            return {"massage":"L'enregistré du musée  à échoué!!!"}, 500
    



class Musee(Resource):
    
    #Recherche d'un musee par son nom
    def get(self, nom):

        mus =  MuseeModel.find_by_nom(nom)

        #Vérification si le nom existe
        if mus:
            return mus.json()
        else:
            return {"massage":"Ce musée n'exsiste pas !!!"}, 404

    #Suppression d'un musée  par son nom
    def delete(self, nom):
        mus =  MuseeModel.find_by_nom(nom)
        
        #Vérification si le nom existe
        if mus:
            mus.delete_to_db()
            return {"massage":"Musée supprimé avec succès !!!"}
        else:
            return {"massage":"Ce Musée n'exsiste pas !!!"}, 404


#pour fair la mise a jour d'un musée
class MuseeUpdate(Resource):
    
    def put(self, id):
        
        mus = MuseeModel.find_by_id(id)
        #si le id existe
        if mus:
            data = MuseeRegister.parser.parse_args()

            if (data['categorie']=="") or (data['nom']=="") or (data['adresse']==""):
                return {"massage":"Veuillez remplir les champs 'categorie', 'nom', 'adresse' SVP !!!"}
            
            mus.categorie = data['categorie']
            mus.nom = data['nom']
            mus.adresse = data['adresse'] 
            mus.contacts = data['contacts'] 
            mus.description = data['description'] 
            mus.site_web = data['site_web']

            try:
                mus.save_to_db()
                return {"massage":"Musée modifié avec succès!!!"}, 201
            except:
                return {"massage":"La modification du musée à échoué!!!"}, 500
        #si le id n'existe pas retourné un message d'eurreur
        else:
            return {"massage":"Ce musée n'exsiste pas !!!"}, 404
        

        


#pour afficher tous les users
class MuseeListe(Resource):
    def get(self):
        return {"Musees": list(map(lambda x: x.json(), MuseeModel.query.all())) }

