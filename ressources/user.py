from flask_restful import Resource, reqparse

from models.user import UserModel


class UserRegister(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('username', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('email', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('password', type = str, required = True, help = 'Ce champ est obligatoire')


    #la méthode post va nous permettre de créer un user
    def post(self):
        data = UserRegister.parser.parse_args()

        if (data['username']=="") or (data['email']=="") or (data['password']==""):
            return {"massage":"Veuillez remplir les champs SVP !!!"}

        #Vérification du username 
        if UserModel.find_by_username(data['username']):
            return {"massage":"Ce utilisateur exsiste déja !!!"}, 400  #si le username existe déja alors on ne peut plus l'utiliser
        
        #Vérification du champ email
        if UserModel.find_by_email(data['email']):
            return {"massage":"Cet email exsiste déja !!!"}, 400  #si le username existe déja alors on ne peut plus l'utiliser
        
        
        user = UserModel(data['username'], data['email'], data['password'])
        
        #Vérification de l'engeristrement dans la base de donnée
        try:
            user.save_to_db()
            return {"massage":"Utilisateur créé avec succès!!!"}, 201
        except:
            return {"massage":"La création de l'utilisateur  à échoué!!!"}, 500
    



class User(Resource):
    
    #Recherche d'un user par son username
    def get(self, username):
        
        #Vérification si le username existe
        if UserModel.find_by_username(username):
            user =  UserModel.find_by_username(username)
            return user.json()
        else:
            return {"massage":"user n'exsiste pas !!!"}, 404

    #Suppression d'un user  par son username
    def delete(self, username):
        
       #Vérification si le username existe
        if UserModel.find_by_username(username):
            user =  UserModel.find_by_username(username)
            user.delete_to_db()
            return {"massage":"user supprimé avec succès !!!"}
        else:
            return {"massage":"user n'exsiste pas !!!"}, 404


#pour fair la mise a jour d'un user
class UserUpdate(Resource):
    
    def put(self, id):
        data = UserRegister.parser.parse_args()
        user = UserModel.find_by_id(id)
        #si le id existe
        if user:

            if (data['username']=="") or (data['email']=="") or (data['password']==""):
                return {"massage":"Veuillez remplir les champs SVP !!!"}
            
            user.username = data['username']
            user.email = data['email']
            user.password = data['password']

            try:
                user.save_to_db()
                return {"massage":"user modifié avec avec succès!!!"}, 201
            except:
                return {"massage":"La modification du user à échoué!!!"}, 500
        #si le id n'existe pas retourné un message d'eurreur
        else:
            return {"massage":"user n'exsiste pas !!!"}, 404
        

        


#pour afficher tous les users
class UserListe(Resource):
    def get(self):
        return {"users": list(map(lambda x: x.json(), UserModel.query.all())) }

