from flask_restful import Resource, reqparse

from models.parcs import ParcModel


class ParcRegister(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('categorie', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('nom', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('adresse', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('contacts', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('description', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('site_web', type = str, required = True, help = 'Ce champ est obligatoire')

    #la méthode post va nous permettre d'enregistrer un parc
    def post(self):
        data = ParcRegister.parser.parse_args()

        if (data['categorie']=="") or (data['nom']=="") or (data['adresse']==""):
            return {"massage":"Veuillez remplir les champs 'categorie', 'nom', 'adresse' SVP !!!"}

        #Vérification pour éviter les doublons 
        if ParcModel.find_by_nom(data['nom']):
            return {"massage":"Ce parc exsiste déja !!!"}, 400  #si le nom existe déja alors on ne peut plus l'utiliser
        
        parc = ParcModel(
            data['categorie'], 
            data['nom'], 
            data['adresse'], 
            data['contacts'], 
            data['description'], 
            data['site_web']
        )
        
        #Vérification de l'engeristrement dans la base de donnée
        try:
            parc.save_to_db()
            return {"massage":"Parc  enregistré avec succès!!!"}, 201
        except:
            return {"massage":"L'enregistré du parc  à échoué!!!"}, 500
    



class Parc(Resource):
    
    #Recherche d'un musee par son nom
    def get(self, nom):

        parc =  ParcModel.find_by_nom(nom)

        #Vérification si le nom existe
        if parc:
            return parc.json()
        else:
            return {"massage":"Ce parc n'exsiste pas !!!"}, 404

    #Suppression d'un musée  par son nom
    def delete(self, nom):
        parc =  ParcModel.find_by_nom(nom)
        
        #Vérification si le nom existe
        if parc:
            parc.delete_to_db()
            return {"massage":"Parc supprimé avec succès !!!"}
        else:
            return {"massage":"Ce parc n'exsiste pas !!!"}, 404


#pour fair la mise a jour d'un musée
class ParcUpdate(Resource):
    
    def put(self, id):
        
        parc = ParcModel.find_by_id(id)
        #si le id existe
        if parc:
            data = ParcRegister.parser.parse_args()

            if (data['categorie']=="") or (data['nom']=="") or (data['adresse']==""):
                return {"massage":"Veuillez remplir les champs 'categorie', 'nom', 'adresse' SVP !!!"}
            
            parc.categorie = data['categorie']
            parc.nom = data['nom']
            parc.adresse = data['adresse'] 
            parc.contacts = data['contacts'] 
            parc.description = data['description'] 
            parc.site_web = data['site_web']

            try:
                parc.save_to_db()
                return {"massage":"Parc modifié avec succès!!!"}, 201
            except:
                return {"massage":"La modification du parc à échoué!!!"}, 500
        #si le id n'existe pas retourné un message d'eurreur
        else:
            return {"massage":"Ce parc n'exsiste pas !!!"}, 404
        

        


#pour afficher tous les users
class ParcListe(Resource):
    def get(self):
        return {"Parcs": list(map(lambda x: x.json(), ParcModel.query.all())) }

