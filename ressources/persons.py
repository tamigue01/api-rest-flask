from flask_restful import Resource, reqparse

from models.persons import PersonModel


class PersonRegister(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('types', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('nom', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('profession', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('fonction_politique', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('description', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('date_naissance', type = str, required = True, help = 'Ce champ est obligatoire')
    parser.add_argument('date_deces', type = str, required = True, help = 'Ce champ est obligatoire')

    #la méthode post va nous permettre de créer un user
    def post(self):
        data = PersonRegister.parser.parse_args()

        if (data['types']=="") or (data['nom']=="") or (data['date_naissance']==""):
            return {"massage":"Veuillez remplir les champs 'types', 'nom', 'date_naissance' SVP !!!"}

        #Vérification pour éviter les doublons 
        if PersonModel.find_by_nom(data['nom']):
            return {"massage":"Cette personnalité exsiste déja !!!"}, 400  #si le nom existe déja alors on ne peut plus l'utiliser
        
        person = PersonModel(
            data['types'], 
            data['nom'], 
            data['profession'], 
            data['fonction_politique'], 
            data['description'], 
            data['date_naissance'],
            data['date_deces']
        )
        
        #Vérification de l'engeristrement dans la base de donnée
        try:
            person.save_to_db()
            return {"massage":"Personnalité  enregistrée avec succès!!!"}, 201
        except:
            return {"massage":"L'enregistré de cette personnalité  à échoué!!!"}, 500
    



class Person(Resource):
    
    #Recherche d'une personnalité par son nom
    def get(self, nom):

        person =  PersonModel.find_by_nom(nom)

        #Vérification si le nom existe
        if person:
            return person.json()
        else:
            return {"massage":"Cette personnalité n'exsiste pas !!!"}, 404

    #Suppression d'une personnalité  par son nom
    def delete(self, nom):
        person =  PersonModel.find_by_nom(nom)
        
        #Vérification si le nom existe
        if person:
            person.delete_to_db()
            return {"massage":"Personnalité supprimée avec succès !!!"}
        else:
            return {"massage":"Cette personnalité n'exsiste pas !!!"}, 404


#pour fair la mise a jour d'une personnalité
class PersonUpdate(Resource):
    
    def put(self, id):
        
        person = PersonModel.find_by_id(id)
        #si le id existe
        if person:
            data = PersonRegister.parser.parse_args()

            if (data['types']=="") or (data['nom']=="") or (data['date_naissance']==""):
                return {"massage":"Veuillez remplir les champs 'types', 'nom', 'date_naissance' SVP !!!"}
            

            person.types = data['types']
            person.nom = data['nom']
            person.profession = data['profession'] 
            person.fonction_politique = data['fonction_politique'] 
            person.description = data['description'] 
            person.date_naissance = data['date_naissance']
            person.date_deces = data['date_deces']

            try:
                person.save_to_db()
                return {"massage":"Personnalité modifiée avec succès!!!"}, 201
            except:
                return {"massage":"La modification de cette personnalité à échoué!!!"}, 500
        #si le id n'existe pas retourné un message d'eurreur
        else:
            return {"massage":"Cette personnalité n'exsiste pas !!!"}, 404
        

        


#pour afficher tous les users
class PersonListe(Resource):
    def get(self):
        return {"Personnalités": list(map(lambda x: x.json(), PersonModel.query.all())) }

